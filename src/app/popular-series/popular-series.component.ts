import { Component, OnInit } from '@angular/core';
import { MoviesService } from '../movies.service';

@Component({
  selector: 'app-popular-series',
  templateUrl: './popular-series.component.html',
  styleUrls: ['./popular-series.component.css']
})
export class PopularSeriesComponent implements OnInit {
	popularList: Array<Object>;

  	constructor(private _moviesService: MoviesService) { 
  		this._moviesService.getPopular().subscribe(res => {
  			this.popularList= res.results;
  		});
  	}

  ngOnInit() {
  }

}
