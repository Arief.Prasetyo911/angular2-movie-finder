import { Component, OnInit } from '@angular/core';
import { MoviesService } from '../movies.service';

@Component({
  selector: 'app-movies',
  templateUrl: './movies.component.html',
  styleUrls: ['./movies.component.css']
})
export class MoviesComponent implements OnInit {
	popularList	: Array<Object>;
	theatersList: Array<Object>;
	topRatesList: Array<Object>;

	searchRes	: Array<Object>;
	searchStr	: string;		//for search keyword

  	constructor(private _moviesService: MoviesService) {
  		this._moviesService.getPopular().subscribe(res => {
  			this.popularList = res.results;
  		});

  		this._moviesService.getInTheaters().subscribe(res => {
  			this.theatersList = res.results;
  		});

  		this._moviesService.getTopRatedMovies().subscribe(res => {
  			this.topRatesList = res.results;
  		})
  	}

  ngOnInit() {
  }

}
