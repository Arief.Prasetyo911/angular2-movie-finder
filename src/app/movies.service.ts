import { Injectable } from '@angular/core';
import { Jsonp, URLSearchParams } from '@angular/http';
import 'rxjs/Rx';

@Injectable()
export class MoviesService {

  key: string;

  	constructor(private _jsonp: Jsonp) {
  		this.key = 'f650112df80f3ce2c11350de8d4679ad';
  		console.log('Movies api ready for action');
   	}

   	//make function start
   	getPopular(){
   		let search = new URLSearchParams();
   		search.set('api_key', this.key);
      search.set('sort_by', 'popularity.desc');
   		return this._jsonp.get('https://api.themoviedb.org/3/discover/movie?callback=JSONP_CALLBACK', {search}).map(res => {return res.json();});
   	}

    getInTheaters(){
       let search = new URLSearchParams();
       search.set('primary_release_date.gt', '2015-10-20');
       search.set('primary_release_date.lte', '2015-10-20');
       search.set('sort_by', 'popularity.desc');
       search.set('api_key', this.key);

       return this._jsonp.get('https://api.themoviedb.org/3/discover/movie?callback=JSONP_CALLBACK', {search}).map(res => { return res.json();});
    }

    getTopRatedMovies(){
      let search = new URLSearchParams();
      search.set('api_key', this.key);

      return this._jsonp.get('https://api.themoviedb.org/3/discover/movie?callback=JSONP_CALLBACK', {search}).map(res => { return res.json();});
    }

    searchMovies(searchStr: string){
      let search = new URLSearchParams();
      search.set('api_key', this.key);
      search.set('sort_by', 'popularity.desc');
      search.set('query', searchStr);

      return this._jsonp.get('https://api.themoviedb.org/3/discover/movie?callback=JSONP_CALLBACK', {search}).map(res => { return res.json();});
    }

    getMovie(id: string){
      var search = new URLSearchParams();
      search.set('api_key', this.key);

      return this._jsonp.get('https://api.themoviedb.org/3/movie/'+id+'?callback=JSONP_CALLBACK', {search}).map(res => { return res.json();});
    }

    getGenres(){
      var search = new URLSearchParams();
      search.set('api_key', this.key);
      search.set('language', 'en-US');

      return this._jsonp.get('https://api.themoviedb.org/3/genre/movie/list?callback=JSONP_CALLBACK', {search}).map(res => { return res.json();});
    }

    getMovieReviews(id: string){
      var search = new URLSearchParams();
      search.set('api_key', this.key);

      return this._jsonp.get('https://api.themoviedb.org/3/movie/'+ id +'/reviews?callback=JSONP_CALLBACK', {search}).map(res => { return res.json();});
    }

    getMovieCredits(id: string){
      var search = new URLSearchParams();
      search.set('api_key', this.key);

      return this._jsonp.get('https://api.themoviedb.org/3/movie/'+ id +'/credits?callback=JSONP_CALLBACK', {search}).map(res => { return res.json();});
    }

    getMovieVideos(id: string){
      var search = new URLSearchParams();
      search.set('api_key', this.key);

      return this._jsonp.get('https://api.themoviedb.org/3/movie/'+ id +'/videos?callback=JSONP_CALLBACK', {search}).map(res => { return res.json();});
    }

    getSimilarMovies(id: string){
      var search = new URLSearchParams();
      search.set('api_key', this.key);

      return this._jsonp.get('https://api.themoviedb.org/3/movie/'+ id +'/similar?callback=JSONP_CALLBACK', {search}).map(res => { return res.json();});
    }

    getMoviesByGenre(id: string){
    	var search = new URLSearchParams();
    	search.set('api_key', this.key);

    	return this._jsonp.get('https://api.themoviedb.org/3/genre/'+id+'/movies?callback=JSONP_CALLBACK', {search}).map(res => { return res.json();});
    }

    getPersonDetail(id: string){
    	var search = new URLSearchParams();
    	search.set('api_key', this.key);

    	return this._jsonp.get('https://api.themoviedb.org/3/person/'+id+'?callback=JSONP_CALLBACK', {search}).
    		map(res => {
    			return res.json();
    		});
    }

    getPersonCast(id: string){
    	var search = new URLSearchParams();
    	search.set('api_key', this.key);

    	return this._jsonp.get('https://api.themoviedb.org/3/person/'+id+'/movie_credits?callback=JSONP_CALLBACK', {search})
    		.map(res => {
    			return res.json();
    		});
    }

    getUpcomingMovies(){
    	var search = new URLSearchParams();
    	search.set('api_key', this.key);

    	return this._jsonp.get('https://api.themoviedb.org/3/movie/upcoming?callback=JSONP_CALLBACK', {search})
    		.map(res => {
    			return res.json();
    		});
    }

}
