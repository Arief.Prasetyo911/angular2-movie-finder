import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { FormsModule } from '@angular/forms';
import { HttpModule, JsonpModule } from '@angular/http';
/*import { RouterModule, Routes } from '@angular/router';*/

import { AppComponent } from './app.component';
import { AppRoutingModule } from './/app-routing.module';
import { PopularSeriesComponent } from './popular-series/popular-series.component';
import { UpcomingComponent } from './upcoming/upcoming.component';
import { MoviesComponent } from './movies/movies.component';
import { MovieComponent } from './movie/movie.component';
import { GenresComponent } from './genres/genres.component';
import { ActorComponent } from './actor/actor.component';


@NgModule({
  declarations: [
    AppComponent,
    PopularSeriesComponent,
    UpcomingComponent,
    MoviesComponent,
    MovieComponent,
    GenresComponent,
    ActorComponent
  ],
  imports: [
    BrowserModule,
    FormsModule,
    HttpModule,
    JsonpModule,
    AppRoutingModule
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
