import { NgModule } from '@angular/core';
import { Routes, RouterModule } 	from '@angular/router';

import { MoviesComponent } from './movies/movies.component';
import { PopularSeriesComponent } from './popular-series/popular-series.component';
import { UpcomingComponent } from './upcoming/upcoming.component';
import { MovieComponent } from './movie/movie.component';
import { GenresComponent } from './genres/genres.component';
import { ActorComponent } from './actor/actor.component';

const routes : Routes = [
	{
		path: '',
		redirectTo: 'home',
		pathMatch: 'full'
	},
	{
		path: 'home',
		component: MoviesComponent,
	},
	{
		path: 'popular',
		component: PopularSeriesComponent
	},
	{
		path: 'upcoming',
		component: UpcomingComponent
	},
	{
		path: 'movie/:id',
		component: MovieComponent
	},
	{
		path: 'genres/:id/:name',
		component: GenresComponent
	},
	{
		path: 'actor/:id',
		component: ActorComponent
	}
];

@NgModule({
  imports: [
    RouterModule.forRoot(routes)
  ],
  exports: [
  	RouterModule
  ]
})
export class AppRoutingModule { }
