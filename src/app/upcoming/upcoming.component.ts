import { Component, OnInit } from '@angular/core';
import { MoviesService } from '../movies.service';

@Component({
  selector: 'app-upcoming',
  templateUrl: './upcoming.component.html',
  styleUrls: ['./upcoming.component.css']
})
export class UpcomingComponent implements OnInit {
	upcomingMovies : Array<Object>;

  	constructor(private _moviesService: MoviesService) { 
  		this._moviesService.getUpcomingMovies().subscribe(res => {
  			this.upcomingMovies = res.results;
  		})
  	}

  ngOnInit() {
  }

}
